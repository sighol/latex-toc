#!/usr/bin/env python

import re
from collections import OrderedDict
import argparse
import os.path

p = argparse.ArgumentParser()
p.add_argument("master_file")
p.add_argument("--nocolor", action="store_true")
args = p.parse_args()

main = args.master_file


def parse(filepath):
    input_sd = {
        'regex': re.compile("\\input{(.*)}"),
        'format': "\\input{{{}}}"
    }

    include_sd = {
        'regex': re.compile("\\include{(.*)}"),
        'format': "\\include{{{}}}",
    }

    recursions = [input_sd, include_sd]
    def parse_rec(filepath):
        try:
            dir = os.path.dirname(filepath)
            with open(filepath, 'r') as file:
                contents = "".join(file.readlines())
                for r in recursions:
                    for file in r['regex'].findall(contents):
                        filepath = os.path.join(dir, file) + ".tex"
                        str = parse_rec(filepath)
                        search  = r['format'].format(file)
                        contents = contents.replace(search, str)
            return contents
        except IOError as e:
            return ""
    return parse_rec(filepath)


def get_dict_index(index, dict):
    last = None
    for i, key in enumerate(dict):
        if key <= index:
            last = key
    return last

class _bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKRED = '\033[31m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def _color_surround(val, color):
    if args.nocolor:
        return val
    return color + str(val) + _bcolors.ENDC

def blue(name):
    return _color_surround(name, _bcolors.OKBLUE)

def red(name):
    return _color_surround(name, _bcolors.OKRED)

def green(name):
    return _color_surround(name, _bcolors.OKGREEN)

def err(name):
    return _color_surround(name, _bcolors.OKRED + _bcolors.FAIL)


def get_command_body(sec, text):
    r = re.compile(r"\\" + sec + r"{(.*?)}")
    out = {}
    for match in r.finditer(text):
        out[match.span()[0]] = match.groups()[0]
    return OrderedDict(sorted(out.items()))


all_tex = parse(main)
chapters = get_command_body('chapter', all_tex)
sections = get_command_body('section', all_tex)
subsections = get_command_body('subsection', all_tex)

class ChapterSection:

    def __init__(self, index, type):
        self.index = index
        self.chapter_index = None
        self.section_index = None
        self.subsection_index = None
        self.type = type

        if type == 'chapter':
            self.chapter_index = index
        elif type == 'section':
            self.chapter_index = get_dict_index(index, chapters)
            self.section_index = index
        elif type == 'subsection':
            self.chapter_index = get_dict_index(index, chapters)
            self.section_index = get_dict_index(index, sections)
            self.subsection_index = index
        if self.chapter_index:
            self.chapter = chapters[self.chapter_index]
        if self.section_index:
            self.section = sections[self.section_index]
        if self.subsection_index:
            self.subsection = subsections[self.subsection_index]

        self.highlights = []

    def add_highlight(self, hl):
        self.highlights.append(hl)

    def __repr__(self):
        out = []
        if self.chapter_index:
            out.append(self.chapter)
        if self.section_index:
            out.append(self.section)
        if self.subsection_index:
            out.append(self.subsection)

        return str(self.index) + " " + " -> ".join(out)

    def __str__(self):
        name = self.chapter
        margin = ''
        out = []
        if self.section_index:
            margin = 4*" "
            name = "- " + (self.section)
        else: # Chapter
            out.append("\n")
        if self.subsection_index:
            margin = 8*" "
            name = "- " + self.subsection

        out.append(margin + name)

        if self.highlights:
            for hl in self.highlights:
                for line in hl:
                    out.append(margin +" "*2 +  red("| ") +line)

        return "\n".join(out)

def gen_chapsecs():
    chapsecs = []
    chapsecs.extend([ChapterSection(x, 'chapter') for x in chapters])
    chapsecs.extend([ChapterSection(x, 'section') for x in sections])
    chapsecs.extend([ChapterSection(x, 'subsection') for x in subsections])

    first_chapsec = ChapterSection(1, None)
    first_chapsec.chapter_index = 1
    first_chapsec.chapter = 'No Chapter'
    chapsecs.append(first_chapsec)

    chapsecs = sorted(chapsecs, key=lambda x: x.index)

    return chapsecs

def get_nearest_chapsec(index, chapsecs):
    last_i = None
    for i, cs in enumerate(chapsecs):
        if cs.index <= index:
            last_i = i
    if last_i != None:
        cs = chapsecs[last_i]
        return cs

def strip_empty_lines_before_after(str):
    lines = str.split("\n")
    lines = [x.replace("\t", 4*" ") for x in lines if x.strip() != ""]
    first_len = len(lines[0]) - len(lines[0].lstrip())
    lines = [x[first_len:] for x in lines]
    return lines


def work(chapsecs):
    environments = ["highlight", "comment"]
    for e in environments:
        add_environment(e, chapsecs)

    for t in ["todo", "hl"]:
        add_command(t, chapsecs)

def add_environment(e, chapsecs):
    e_begin_rx = re.compile(r"\\begin{" + e + "}")
    e_end_rx = re.compile(r"\\end{" + e + "}")

    m_begin = e_begin_rx.finditer(all_tex)
    m_end = e_end_rx.finditer(all_tex)
    for begin, end in zip(m_begin, m_end):
        start_index = begin.span()[1]
        end_index = end.span()[0]

        chapsec = get_nearest_chapsec(start_index, chapsecs)
        matched_text = all_tex[start_index:end_index]
        matched_text = strip_empty_lines_before_after(matched_text)
        if chapsec:
            chapsec.add_highlight(matched_text)

def add_command(t, chapsecs):
    prog = re.compile(r"\\" + t + r"{(.*)}")
    for m in prog.finditer(all_tex):
        chapsec = get_nearest_chapsec(m.span()[0], chapsecs)
        if chapsec:
            chapsec.add_highlight([blue(t) + " " +m.groups()[0]])

chapsecs = gen_chapsecs()
work(chapsecs)

for cs in chapsecs:
    print(cs)

